# File:    Makefile
# Purpose: Makefile for Omni
# Author:  Ben Laughton
#
# History:
#
# 2000-03-02: BAL
# Created.




# ------------------------------------------------------------------------------
# Paths
#

EXP_HDR         = <export$dir>.^.Interface2
EXP_C_H         = <Cexport$dir>.Interface.h

LIBDIR          = <Lib$Dir>

RESFSDIR        = Resources.OmniClient
INSTAPP         = $(INSTDIR).!Omni

# ------------------------------------------------------------------------------
# Generic options
#

MKDIR           = do mkdir -p
AS              = objasm
CC              = $(MemCheckCC) cc
C++             = c++
CMHG            = cmhg
CP              = copy
LD              = $(MemCheckLD) link
RM              = remove
WIPE            = x wipe
CD              = dir
CHMOD           = access
TOUCH           = create
MODSQZ          = modsqz
AWK             = GNU.gawk 
INSERTVERSION   = ${AWK} -f Build:AwkVers


# ------------------------------------------------------------------------------
# Flags
#

ASFLAGS         += -depend !Depend $(THROWBACK) -stamp -quit
CFLAGS          += -depend !Depend $(THROWBACK) $(CINCLUDES) $(CDEFINES) -c -Wp -zM -ffah -strict
C++FLAGS        += -depend !Depend $(THROWBACK) $(CINCLUDES) $(CDEFINES) -c -Wp -zM -ffa +a1
CMHGFLAGS       = -depend !Depend $(THROWBACK) $(CINCLUDES) $(CDEFINES) -p
LDFLAGS         =
CPFLAGS         = ~cfr~v
WFLAGS          = ~c~vr
CHMOCDEFINES    = RW/r

CDEFINES        += -DINET -DCOMPAT_INET4 $(EXPERT)

CINCLUDES       = -IC:,TCPIPLibs:




# ------------------------------------------------------------------------------
# Libraries
#

CLIB            = CLIB:o.stubs
RLIB            = RISCOSLIB:o.risc_oslib
RSTUBS          = RISCOSLIB:o.rstubs
ROMSTUBS        = RISCOSLIB:o.romstubs
ROMCSTUBS       = RISCOSLIB:o.romcstubs
ABSSYM          = RISC_OSLib:o.c_AbsSym

DEBUGLIB        = $(LIBDIR).DebugLib.o.DebugLibzm
INETLIB         = TCPIPLibs:o.inetlibzm
SOCKLIB         = TCPIPLibs:o.socklibzm
#DESKLIB        = $(LIBDIR).Desk.o.Desk_D $(LIBDIR).Desk.o.Debug.tml
DESKLIB         = $(LIBDIR).Desk.o.Desk_M
ASMUTILS        = $(LIBDIR).AsmUtils.o.AsmUtilszm

# Allow libraries to vary according to the type of build, distingusihed by
# whatever "extension" is being used for the object code files.  This is
# because the old trick of (probably) DebugLib adding lots to the size of a
# module, even when none of the code in it is referenced seems to have come
# back.
L_o   = $(DESKLIB) $(INETLIB) $(ASMUTILS)
L_od  = $(DEBUGLIBS) $(DEBUGLIB) $(INETLIB) $(SOCKLIB) $(DESKLIB) $(ASMUTILS)
L_oMC = $(DEBUGLIB) $(INETLIB) $(SOCKLIB) $(DESKLIB) $(ASMUTILS)

LIBS = $(L_$(EXT))

include ModuleLibs


# ------------------------------------------------------------------------------
# Program specific options
#

COMPONENT       = OmniClient
TARGET         ?= Omni
RAMTARGET       = rm.$(TARGET)
ROMTARGET       = aof.$(TARGET)

ROMOBJS =\
 $(EXT).Client\
 $(EXT).Close\
 $(EXT).CoreInit\
 $(EXT).Events\
 $(EXT).ExtLib\
 $(EXT).ExtLibAsm\
 $(EXT).Filer\
 $(EXT).FileType\
 $(EXT).Heap\
 $(EXT).List\
 $(EXT).MakeError\
 $(EXT).Mount\
 $(EXT).OmniClient\
 $(EXT).Omni\
 $(EXT).Parse\
 $(EXT).Print\
 $(EXT).PrintFS\
 $(EXT).Redraw\
 $(EXT).Sort\
 $(EXT).Time\
 $(EXT)._Args\
 $(EXT)._Close\
 $(EXT)._Errs\
 $(EXT)._File\
 $(EXT)._Func\
 $(EXT)._GBPB\
 $(EXT)._GetBytes\
 $(EXT)._Open\
 $(EXT)._PutBytes\
 $(EXT)._Veneers\

RAMOBJS =\
 $(ROMOBJS)\


# $(EXT).resources

HDR = h.OmniClient




# ------------------------------------------------------------------------------
# Rule patterns
#

.SUFFIXES:      .o .od .oHP .oMC .i .s .c++ .c .cmhg
.c.i:;          $(CC)   $(CFLAGS)       -C -E $< >> $@
.c.o:;          $(CC)   $(CFLAGS)       -o $@ $<
.c.od:;         $(CC)   $(CFLAGS)       -fn -o $@ $<
.c.oMC:;        $(CC)   $(CFLAGS)       -fn -g -o $@ $<
#.c.oMC:;       $(CC)   $(CFLAGS)       -o $@ $<
.c.oHP:;        $(CC)   $(CFLAGS)       -fn -o $@ $<
.cmhg.i:;       @echo $@
.cmhg.o:;       $(CMHG) $(CMHGFLAGS)    $< -o $@
.cmhg.od:;      $(CMHG) $(CMHGFLAGS)    $< -o $@
.cmhg.oMC:;     $(CMHG) $(CMHGFLAGS)    $< -o $@
.cmhg.oHP:;     $(CMHG) $(CMHGFLAGS)    $< -o $@
.s.i:;          @echo $@
.s.o:;          $(AS)   $(ASFLAGS)      $< $@
.s.od:;         $(AS)   $(ASFLAGS)      $< $@
.s.oMC:;        $(AS)   $(ASFLAGS)      $< $@
.s.oHP:;        $(AS)   $(ASFLAGS)      $< $@



# ------------------------------------------------------------------------------
# Local build rules
#

all: $(RAMTARGET)

debug: $(RAMTARGET)

memcheck: $(RAMTARGET)

preprocess: $(ROMTARGET)

$(RAMTARGET): $(RAMOBJS) $(LIBS) $(HDR) $(EXT).local_dirs
	$(MKDIR) rm
	$(LD) -rmf $(LDFLAGS) -o $@ $(RAMOBJS) $(LIBS) $(CLIB)
	$(MODSQZ) $@

$(EXT).resources: LocalRes:Messages
	ResGen messages_data $@ LocalRes:Messages Resources.$(COMPONENT).Messages

$(HDR): cmhg.OmniClient VersionNum C:Global.h.Services
	$(CMHG) $(CMHGFLAGS) cmhg.OmniClient -d $@




# ------------------------------------------------------------------------------
# Common build rules

$(EXT).local_dirs:
	$(MKDIR) $(EXT)
	$(TOUCH) $@




# ------------------------------------------------------------------------------
# RISC OS ROM build rules
#

rom: $(ROMTARGET)
	@echo $(COMPONENT): rom complete

export: hdr.OmniClient cmhg.OmniClient
	$(MKDIR) o
	$(CMHG) -p -d h.OmniClientHdr cmhg.OmniClient
	do $(AWK) -- "/.ifndef Omni/,/endif/" h.OmniClientHdr > o._h_Omni
	do <Perl$Dir>.perl Build:Hdr2H hdr.OmniClient $(EXP_C_H).OmniClient
	FAppend $(EXP_C_H).OmniClient $(EXP_C_H).OmniClient o._h_Omni
	$(CP) hdr.OmniClient $(EXP_HDR).OmniClient $(CPFLAGS)
	@echo $(COMPONENT): header export complete

install: $(RAMTARGET)
	$(MKDIR) $(INSTAPP).Utils
	$(MKDIR) $(INSTAPP).Utils.Access
	$(MKDIR) $(INSTAPP).RMStore
	$(MKDIR) $(INSTAPP).Themes.Ursula
	$(MKDIR) $(INSTAPP).Themes.Morris4
	$(MKDIR) $(INSTAPP).Resources.${LOCALE}
	$(CP) Resources.!Boot      $(INSTAPP).!Boot                          $(CPFLAGS)
	$(CP) Resources.HourOn     $(INSTAPP).Utils.HourOn                   $(CPFLAGS)
	$(CP) Resources.HourOff    $(INSTAPP).Utils.HourOff                  $(CPFLAGS)
	$(CP) Resources.KillTask   $(INSTAPP).Utils.Access.KillTask          $(CPFLAGS)
	$(CP) Resources.Dormant    $(INSTAPP).Utils.Access.Dormant           $(CPFLAGS)
	$(CP) LocalRes:!Help       $(INSTAPP).!Help                          $(CPFLAGS)
	$(CP) LocalRes:!Run        $(INSTAPP).!Run                           $(CPFLAGS)
	$(CP) LocalRes:Files       $(INSTAPP).Files                          $(CPFLAGS)
	$(CP) $(RAMTARGET)         $(INSTAPP).RMStore.$(COMPONENT)           $(CPFLAGS)
	$(CP) LocalRes:Templates   $(INSTAPP).Resources.${LOCALE}.Templates  $(CPFLAGS)
	$(CP) LocalRes:Template3D  $(INSTAPP).Resources.${LOCALE}.Template3D $(CPFLAGS)
	${INSERTVERSION} LocalRes:Messages > $(INSTAPP).Resources.${LOCALE}.Messages
	$(CP) Resources.!Sprites           $(INSTAPP).Themes.!Sprites           $(CPFLAGS)
	$(CP) Resources.!Sprites11         $(INSTAPP).Themes.!Sprites11         $(CPFLAGS)
	$(CP) Resources.!Sprites22         $(INSTAPP).Themes.!Sprites22         $(CPFLAGS)
	$(CP) Resources.Ursula.!Sprites    $(INSTAPP).Themes.Ursula.!Sprites    $(CPFLAGS)
	$(CP) Resources.Ursula.!Sprites22  $(INSTAPP).Themes.Ursula.!Sprites22  $(CPFLAGS)
	$(CP) Resources.Morris4.!Sprites   $(INSTAPP).Themes.Morris4.!Sprites   $(CPFLAGS)
	$(CP) Resources.Morris4.!Sprites22 $(INSTAPP).Themes.Morris4.!Sprites22 $(CPFLAGS)
	# For LanManFS
	$(CP) Resources.LanMan             $(INSTAPP).Themes.LanMan             $(CPFLAGS)
	$(CP) Resources.Ursula.LanMan      $(INSTAPP).Themes.Ursula.LanMan      $(CPFLAGS)
	$(CP) Resources.Morris4.LanMan     $(INSTAPP).Themes.Morris4.LanMan     $(CPFLAGS)
	# For NetFiler
	$(CP) Resources.Net                $(INSTAPP).Themes.Net                $(CPFLAGS)
	$(CP) Resources.Ursula.Net         $(INSTAPP).Themes.Ursula.Net         $(CPFLAGS)
	$(CP) Resources.Morris4.Net        $(INSTAPP).Themes.Morris4.Net        $(CPFLAGS)
	# For OmniAccess
	$(CP) Resources.Share              $(INSTAPP).Themes.Share              $(CPFLAGS)
	$(CP) Resources.Ursula.Share       $(INSTAPP).Themes.Ursula.Share       $(CPFLAGS)
	$(CP) Resources.Morris4.Share      $(INSTAPP).Themes.Morris4.Share      $(CPFLAGS)
	# For OmniNFS
	$(CP) Resources.NFS                $(INSTAPP).Themes.NFS                $(CPFLAGS)
	$(CP) Resources.Ursula.NFS         $(INSTAPP).Themes.Ursula.NFS         $(CPFLAGS)
	$(CP) Resources.Morris4.NFS        $(INSTAPP).Themes.Morris4.NFS        $(CPFLAGS)
	@echo $(COMPONENT): $(RAMTARGET) installed

install_rom: $(ROMTARGET)
	$(CP) $(ROMTARGET) $(INSTAPP).$(COMPONENT) $(CPFLAGS)
	@echo $(COMPONENT): rom module installed

clean:
	$(WIPE) $(EXT)  $(WFLAGS)


# ROM target (re-linked at ROM Image build time)

$(ROMTARGET): $(ROMOBJS) $(LIBS) $(ROMCSTUBS) $(EXT).local_dirs
	$(MKDIR) aof
	$(LD) -aof $(LDFLAGS) -o $@ $(ROMOBJS) $(LIBS) $(ROMCSTUBS)


# Final link for the ROM Image (using given base address)

rom_link:
	$(MKDIR) linked
	$(LD) $(LDFLAGS) -o linked.$(COMPONENT) -map -rmf -base $(ADDRESS) $(ROMTARGET) $(ABSSYM) > map.$(COMPONENT)
	$(CP) linked.$(COMPONENT) $(LINKDIR).$(COMPONENT) $(CPFLAGS)
	@echo $(COMPONENT): rom_link complete

# ------------------------------------------------------------------------------
# Dynamic dependencies:
