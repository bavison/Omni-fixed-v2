/* Copyright 2000 Pace Micro Technology plc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Program:	_File.c - core application routines
 *
 * Project:	!Omni OmniClient(tm) project
 *
 * Author:	Nick Smith
 *              ANT Limited
 *              Cambridge
 *              Internet: nas@ant.co.uk
 *
 * Date:	23 October 1994
 * Last Edited:	23 December 1994
 *
 * Copyright 1994 by ANT Limited
 */

#include <stddef.h>

#include <kernel.h>

#include <DebugLib/DebugLib.h>

#include "_Errs.h"
#include "_Veneers.h"

/*  OS_File entry points.
 */
extern _kernel_oserror *fsentry_file(FSEntry_File_Parameter *parm)
{
  _kernel_oserror *err = NULL;

  dprintf (("", "fsentry_file: %x\n", parm->reason));
  switch (parm->reason)
  {
    case FSEntry_File_Reason_WriteCatalogueInformation:
      /* Do nothing */
      break;

    case FSEntry_File_Reason_ReadCatalogueInformation:
      dprintf (("", "fsentry_file: get cat '%s' '%s'\n",
        parm->name ? parm->name : "",
        parm->special_field ? parm->special_field : ""));
      parm->reason = (FSEntry_File_Reason) 0; /* Not found */
      break;

    case FSEntry_File_Reason_CreateFile:
      dprintf (("", "fsentry_file: create file '%s' '%s'\n",
        parm->name ? parm->name : "",
        parm->special_field ? parm->special_field : ""));
      break;

    default:
      dprintf (("", "fsentry_file: not supported\n"));
      err = &mb_nfserr_DummyFSDoesNothing;
      break;
  }
  return(err);
}
